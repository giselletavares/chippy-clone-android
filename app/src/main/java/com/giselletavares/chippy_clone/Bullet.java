package com.giselletavares.chippy_clone;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;

public class Bullet
{
    private Rect bullet;
    private Point target;
    private Rect targetHitBox;
    private int speed;

    public Bullet(Rect bullet)
    {
        this.bullet = bullet;
    }

    public Bullet(Rect bullet, Point target)
    {
        this(bullet);
        this.target = target;
        this.targetHitBox = new Rect(this.target.x, this.target.y, this.target.x + 1, this.target.y + 1);
    }

    public Bullet(Rect bullet, Point target, int speed)
    {
        this(bullet, target);
        this.speed = speed;
    }

    public Rect getBullet() { return bullet; }
    public void setBullet(Rect bullet) { this.bullet = bullet; }
    public Point getTarget() { return target; }
    public void setTarget(Point target) { this.target = target; }
    public int getSpeed() { return speed; }
    public void setSpeed(int speed) { this.speed = speed; }

    public void updatePosition()
    {
        this.moveTowards(this.target);
    }

    public void moveTowards(Point moveHere)
    {
        double a = (moveHere.x - this.bullet.left);
        double b = (moveHere.y - this.bullet.top);
        double distance = Math.sqrt((a*a) + (b*b));

        // 2. calculate the "rate" to move
        double xn = (a / distance);
        double yn = (b / distance);

        // 3. move the bullet
        boolean stillMoveX = true;
        boolean stillMoveY = true;

        if(a < 0 && (this.bullet.left + (int)(xn * this.speed)) < moveHere.x) stillMoveX = false;
        if(a > 0 && (this.bullet.left + (int)(xn * this.speed)) > moveHere.x) stillMoveX = false;

        if(b < 0 && (this.bullet.top + (int)(yn * this.speed)) < moveHere.y) stillMoveY = false;
        if(b > 0 && (this.bullet.top + (int)(yn * this.speed)) > moveHere.y) stillMoveY = false;

        this.bullet.left = stillMoveX ? this.bullet.left + (int)(xn * this.speed) : moveHere.x;
        this.bullet.top = stillMoveY ? this.bullet.top + (int)(yn * this.speed) : moveHere.y;
        this.bullet.right = this.bullet.left + 1;
        this.bullet.bottom = this.bullet.top + 1;
    }

    public boolean isBulletOnDestination() { return this.bullet.intersect(this.targetHitBox); }
}
