package com.giselletavares.chippy_clone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MenuActivity extends AppCompatActivity {
    protected AudioApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        // Make the screen FullScreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_menu);
        app = (AudioApplication) getApplication();
        app.mediaPlayer.start();
    }

    public void sendMessage (View view) throws Throwable {
        switch (view.getId()) {
            case R.id.buttonPlay: {
                Intent intent = new Intent(MenuActivity.this, MainActivity.class);
                startActivity(intent);
                break;
            }

//            case R.id.buttonSettings: {
//                app.mediaPlayer.stop();
//            }

            case R.id.buttonExit: {
                app.mediaPlayer.stop();
                finish();
                break;
            }
        }
    }
}