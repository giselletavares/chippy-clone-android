package com.giselletavares.chippy_clone;

        import android.app.Application;
        import android.content.res.AssetFileDescriptor;
        import android.content.res.AssetManager;
        import android.media.AudioManager;
        import android.media.MediaPlayer;

        import java.io.IOException;

public class AudioApplication extends Application {
    MediaPlayer mediaPlayer;

    @Override
    public void onCreate() {
        super.onCreate();

        // ==============================================
        // BACKGROUND MUSIC==============================
        // ==============================================
//        getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mediaPlayer = new MediaPlayer();
        try {
            AssetManager assetManager = getAssets();
            AssetFileDescriptor descriptor = assetManager.openFd("spacefleetrejects.ogg");
            mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            mediaPlayer.prepare();
            mediaPlayer.setLooping(true);
        }
        catch (IOException e) {
            System.out.print("Can't load Music File" + e.getMessage());
            mediaPlayer = null;
        }
    }
}
