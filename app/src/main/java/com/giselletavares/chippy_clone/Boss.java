package com.giselletavares.chippy_clone;

import android.content.Context;

public class Boss extends Sprite
{
    private int hp;

    public Boss(Context c, int xPosition, int yPosition, int w, int h) {
        super(c, xPosition, yPosition, w, h);
    }

    public Boss(Context c, int xPosition, int yPosition, int drawable) {
        super(c, xPosition, yPosition, drawable);
    }

    public Boss(Context c, int xPosition, int yPosition, int w, int h, LayerPixelColor pixelBossLayerColor) {
        super(c, xPosition, yPosition, w, h, pixelBossLayerColor);
    }

    public int getHp() { return hp; }

    public void setHp(int hp) { this.hp = hp; }
}
