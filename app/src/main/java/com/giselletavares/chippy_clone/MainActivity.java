package com.giselletavares.chippy_clone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

import java.io.IOException;

public class MainActivity extends AppCompatActivity
{
    GameEngine chippyGame;

    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // Make the screen FullScreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Get size of the screen
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);


        // ==============================================
        // BACKGROUND MUSIC==============================
        // ==============================================
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        mediaPlayer = new MediaPlayer();
        try {
            AssetManager assetManager = getAssets();
            AssetFileDescriptor descriptor = assetManager.openFd("spacefleetrejects.ogg");
            mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            mediaPlayer.prepare();
            mediaPlayer.setLooping(true);
        }
        catch (IOException e) {
            System.out.print("Can't load Music File" + e.getMessage());
            mediaPlayer = null;
        }

        // Initialize the GameEngine object
        // Pass it the screen size (height & width)
        // 256 is the height escape for landscape
        // 283 is the height escape for portrait
        chippyGame = new GameEngine(this, size.x, size.y);

        // Make GameEngine the view of the Activity
        setContentView(chippyGame);

        chippyGame.startGame();
    }

    // Android Lifecycle function
    @Override
    protected void onResume()
    {
        super.onResume();
        if (mediaPlayer != null) {
            mediaPlayer.start();
        }
        chippyGame.resumeGame();
    }

    // Stop the thread in gameEngine
    @Override
    protected void onPause()
    {
        super.onPause();
        mediaPlayer.pause();
        chippyGame.pauseGame();
    }
}
